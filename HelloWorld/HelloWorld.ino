/*
  Hello World

  This program outputs the string "Hello, World!\n" to the serial
  port (9600 baud) and blinks the LED tied to pin 13 once a second

  by Jacob Kunnappally
*/

#define BOARD_LED_PIN 13
#define WAIT_TIME 1000

bool pinValue = LOW;

void setup() 
{
    Serial.begin(9600);
    pinMode(BOARD_LED_PIN, OUTPUT);
}

void loop() 
{
    digitalWrite(BOARD_LED_PIN, pinValue);
    pinValue = !pinValue;
    Serial.println("Hello, World!");
    delay(WAIT_TIME);
}

